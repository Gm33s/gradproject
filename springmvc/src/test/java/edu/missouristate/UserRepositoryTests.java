package edu.missouristate;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.test.annotation.Rollback;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Rollback(false)
public class UserRepositoryTests {

@Autowired
private UserRepository repo;

@Autowired
private TestEntityManager entityManager;

@Test
public void testCreateUser()
{
	User user = new User();
	user.setName("Batman");
	user.setMobileNo("7011545756");
	user.setEmail("bats@aa.com");
	user.setPassword("Passw0rd");
	
	User savedUser = repo.save(user);
	
	
	User existUser = entityManager.find(User.class, savedUser.getId());
	
	assertThat(existUser.getEmail()).isEqualTo(user.getEmail());
	
}

public interface UserRepository extends JpaRepository<User, Long> {
    @Query("SELECT u FROM User u WHERE email = ?1")
    public User findByEmail(String email);
     
}
}

