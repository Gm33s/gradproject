package edu.missouristate.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import edu.missouristate.User;
import edu.missouristate.UserRepository;

@Controller
public class SpringMvcController {
	
	@Autowired
	private UserRepository repo;

	
	@GetMapping(value="/") 
	public String getIndexPage() {
		return "index.html";
	}
	
	@GetMapping(value="/register") 
	public String signup() {
		return "signup.html";
	}
	
	@PostMapping("/process_register")
	public String processRegister(User user) {
	     
	    repo.save(user);
	     
	    return "register_success";
	}
		
}
